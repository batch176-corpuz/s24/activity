//find users with letter y in firstname or lastname
//show only email, isAdmin
db.users.find(
	{
		$or: [
			{ firstName: { $regex: "y", $options: "$i" } },
			{ lastName: { $regex: "y", $options: "$i" } },
		],
	},
	{
		_id: 0,
		email: 1,
		isAdmin: 1,
	}
);

//find users with letter e in firstname and is an admin
//show only email, isAdmin
db.users.find(
	{
		$and: [
			{ firstName: { $regex: "e", $options: "$i" } },
			{ isAdmin: true },
		],
	},
	{
		_id: 0,
		email: 1,
		isAdmin: 1,
	}
);

//find products with letter x in name, price >= 50000
db.products.find({
	$and: [
		{ name: { $regex: "x", $options: "$i" } },
		{ price: { $gte: 50000 } },
	],
});

//update all products with price < 2000 to inactive
db.products.updateMany(
	{
		price: { $lt: 2000 },
	},
	{
		$set: {
			isActive: false,
		},
	}
);

//delete all products with price greater than 20000
db.products.deleteMany({
	price: { $gt: 20000 },
});
